// console.log("duong cute lac loi");
const TODOLIST_LOCALSTORAGE = "TODOLIST";
let todo = [];

const COMPLETEDLIST_LOCALSTORAGE = "COMPLETEDLIST";
let completed = [];

// check data dưới localStorage
let todoListJson = localStorage.getItem(TODOLIST_LOCALSTORAGE);
let completedListJson = localStorage.getItem(COMPLETEDLIST_LOCALSTORAGE);

if (todoListJson != null || completedListJson != null) {
  todo = JSON.parse(todoListJson).map((item) => item);
  completed = JSON.parse(completedListJson).map((item) => item);

  render();
}

// xoá data trùng nhau của todo và completed ở mảng completed
function validate() {
  todo.map((item1) => {
    let index = completed.findIndex((item2) => item2 === item1);

    if (index != -1) {
      completed.splice(index, 1);
    }
  });
}

// lấy dữ liệu từ form
document.querySelector("#addItem").onclick = function () {
  let newTask = document.querySelector("#newTask").value;
  if (newTask == "") {
    alert("nhập nhiệm vụ vào đi bạn êh");
  } else {
    document.querySelector("#newTask").value = "";

    todo.push(newTask);
    validate();

    render();
  }
};

// chuyển dữ liệu từ uncheck thành checked
function uncheck(name) {
  let index = todo.findIndex((item) => item === name);

  // lấy item đó ra và thêm vào completed
  let item = todo.slice(index, index + 1);
  completed.push(item[0]);
  // xoá item ở to do List
  todo.splice(index, 1);

  render();
}

// chuyển dữ liệu từ checked thành uncheck
function checked(name) {
  let index = completed.findIndex((item) => item === name);

  // lấy item đó ra và thêm vào completed to do List
  let item = completed.slice(index, index + 1);
  todo.push(item[0]);
  // xoá item ở completed
  completed.splice(index, 1);

  render();
}

// xoá item
function xoa(name) {
  let index1 = completed.findIndex((item) => item === name);
  let index2 = todo.findIndex((item) => item === name);

  if (index1 != -1) {
    completed.splice(index1, 1);
  } else if (index2 != -1) {
    todo.splice(index2, 1);
  } else {
    alert("Hông tìm thấy");
  }
  render();
}

// render data
function render() {
  let contentToDo = "";
  let contentCompleted = "";

  todo.forEach((item) => {
    contentToDo += /*html*/ `
      <li>
      <span>${item}</span>
      <div class="buttons">
        <button class="remove" onclick="xoa('${item}')">
          <i class="fa-regular fa-trash-can"></i>
        </button>
        <button class="complete uncheck" onclick="uncheck('${item}')">
          <i class="fa-regular fa-circle-check"></i>
        </button>
        <button class="complete checked" onclick="checked('${item}')">
          <i class="fa-solid fa-circle-check"></i>
        </button>
      </div>
    </li>
    `;
  });
  document.querySelector("#todo").innerHTML = contentToDo;

  completed.forEach((item) => {
    contentCompleted += /*html*/ `
        <li>
        <span>${item}</span>
        <div class="buttons">
          <button class="remove" onclick="xoa('${item}')">
            <i class="fa-regular fa-trash-can"></i>
          </button>
          <button class="complete uncheck" onclick="uncheck('${item}')">
            <i class="fa-regular fa-circle-check"></i>
          </button>
          <button class="complete checked" onclick="checked('${item}')">
            <i class="fa-solid fa-circle-check"></i>
          </button>
        </div>
      </li>
      `;
  });
  document.querySelector("#completed").innerHTML = contentCompleted;

  // đẩy data xuống localStorage
  localStorage.setItem(TODOLIST_LOCALSTORAGE, JSON.stringify(todo));
  localStorage.setItem(COMPLETEDLIST_LOCALSTORAGE, JSON.stringify(completed));
}

// hàm sort and filter
document.querySelector("#one").onclick = function () {
  document.querySelector("#todo").classList.toggle("d-none");
};

document.querySelector("#two").onclick = function () {
  todo.sort();
  completed.sort();
  render();
};

document.querySelector("#three").onclick = function () {
  todo.sort();
  todo.reverse();

  completed.sort();
  completed.reverse();
  render();
};

document.querySelector("#all").onclick = function () {
  document.querySelector("#todo").classList.remove("d-none");
};
